package main

import (
	"fmt"
	"gitlab.com/tvacula/golang-course-todd-mcleod/13-jedi-ninja/01/dog"
)

type canine struct {
	name string
	age  int
}

func main() {
	fido := canine{
		name: "Fido",
		age:  dog.Years(10),
	}
	fmt.Println(fido)
	fmt.Println(dog.YearsTwo(20))
}
