package dog

import (
	"fmt"
	"testing"
)

func TestYears(t *testing.T) {
	type testData struct {
		data     int
		expected int
	}

	provider := []testData{
		{1, 7},
		{2, 14},
		{3, 21},
		{10, 70},
		{1111, 7777},
	}

	for _, testValue := range provider {
		result := Years(testValue.data)
		if result != testValue.expected {
			t.Error("Expected", testValue.expected, "Got", result)
		}
	}
}

func TestYearsTwo(t *testing.T) {
	type testData struct {
		data     int
		expected int
	}

	provider := []testData{
		{1, 7},
		{2, 14},
		{3, 21},
		{10, 70},
		{1111, 7777},
	}

	for _, testValue := range provider {
		result := YearsTwo(testValue.data)
		if result != testValue.expected {
			t.Error("Expected", testValue.expected, "Got", result)
		}
	}
}

func ExampleYears() {
	fmt.Println(Years(1))
	// Output:
	// 7
}

func ExampleYearsTwo() {
	fmt.Println(YearsTwo(1))
	// Output:
	// 7
}

func BenchmarkYears(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Years(5)
	}
}

func BenchmarkYearsTwo(b *testing.B) {
	for i := 0; i < b.N; i++ {
		YearsTwo(5)
	}
}
