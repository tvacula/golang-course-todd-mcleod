package word

import (
	"fmt"
	"reflect"
	"testing"
)

func TestCount(t *testing.T) {
	type testData struct {
		data     string
		expected int
	}

	provider := []testData{
		{"Hello there", 2},
		{"General Kenobi!", 2},
		{"Nice day for fishin' ain't it? He he", 8},
		{"For peace in the kingdom!", 5},
	}

	for _, tv := range provider {
		x := Count(tv.data)
		if x != tv.expected {
			t.Error("Expected", tv.expected, "Got", x)
		}
	}
}

func TestUseCount(t *testing.T) {
	expectedResult := map[string]int{
		"Nice":    1,
		"day":     1,
		"for":     1,
		"fishin'": 1,
		"ain't":   1,
		"it?":     1,
		"he":      2,
	}

	x := UseCount("Nice day for fishin' ain't it? he he")
	if reflect.DeepEqual(x, expectedResult) == false {
		t.Error("Expected", expectedResult, "Got", x)
	}
}

func ExampleCount() {
	fmt.Println(Count("Nice day for fishin' ain't it? He he"))
	// Output:
	// 8
}

func ExampleUseCount() {
	fmt.Println(UseCount("Nice day for fishin' ain't it? he he"))
	// Output:
	// map[Nice:1 ain't:1 day:1 fishin':1 for:1 he:2 it?:1]
}

func BenchmarkCount(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Count("Robert Cohn was once middleweight boxing champion of Princeton. Do not think that I am very much impressed by that as a boxing title, but it meant a lot to Cohn. He cared nothing for boxing, in fact he disliked it, but he learned it painfully and thoroughly to counteract the feeling of inferiority and shyness he had felt on being treated as a Jew at Princeton. There was a certain inner comfort in knowing he could knock down anybody who was snooty to him, although, being very shy and a thoroughly nice boy, he never fought except in the gym. He was Spider Kelly's star pupil. Spider Kelly taught all his young gentlemen to box like featherweights, no matter whether they weighed one hundred and five or two hundred and five pounds. But it seemed to fit Cohn. He was really very fast. He was so good that Spider promptly overmatched him and got his nose permanently flattened.")
	}
}
func BenchmarkUseCount(b *testing.B) {
	for i := 0; i < b.N; i++ {
		UseCount("Robert Cohn was once middleweight boxing champion of Princeton. Do not think that I am very much impressed by that as a boxing title, but it meant a lot to Cohn. He cared nothing for boxing, in fact he disliked it, but he learned it painfully and thoroughly to counteract the feeling of inferiority and shyness he had felt on being treated as a Jew at Princeton. There was a certain inner comfort in knowing he could knock down anybody who was snooty to him, although, being very shy and a thoroughly nice boy, he never fought except in the gym. He was Spider Kelly's star pupil. Spider Kelly taught all his young gentlemen to box like featherweights, no matter whether they weighed one hundred and five or two hundred and five pounds. But it seemed to fit Cohn. He was really very fast. He was so good that Spider promptly overmatched him and got his nose permanently flattened.")
	}
}
