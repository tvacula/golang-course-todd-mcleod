package main

import (
	"fmt"
	"gitlab.com/tvacula/golang-course-todd-mcleod/13-jedi-ninja/02/quote"
	"gitlab.com/tvacula/golang-course-todd-mcleod/13-jedi-ninja/02/word"
)

func main() {
	fmt.Println(word.Count(quote.SunAlso))

	for k, v := range word.UseCount(quote.SunAlso) {
		fmt.Println(v, k)
	}
}
