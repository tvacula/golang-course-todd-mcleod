package mymath

import "testing"

func TestCenteredAvg(t *testing.T) {
	type testData struct {
		data   []int
		result float64
	}

	provider := []testData{
		{[]int{1, 4, 3, 2, 5}, 3},
		{[]int{8, 6, 9, 5, 7}, 7},
	}

	for _, v := range provider {
		x := CenteredAvg(v.data)
		if x != v.result {
			t.Error("Expected", v.result, "Got", x)
		}
	}
}

func BenchmarkCenteredAvg(b *testing.B) {
	for i := 0; i < b.N; i++ {
		CenteredAvg([]int{1, 4, 3, 2, 5, 6, 7, 8, 9, 10})
	}
}
