package main

import (
	"fmt"
	"math/rand"
)

func main() {
	x := rand.Int31n(100)

	if x == 0 {
		fmt.Println("x is zero")
	} else if x > 0 && x < 50 {
		fmt.Println("x not zero and less than 50")
	} else {
		fmt.Printf("x is %v\n", x)
	}
}
