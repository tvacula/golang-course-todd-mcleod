package main

import (
	"fmt"
	"math/rand"
)

func main() {
	x := rand.Int31n(100)

	if x == 0 {
		fmt.Println("x is zero")
	}
}
