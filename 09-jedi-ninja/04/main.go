package main

import (
	"fmt"
	"sync"
)

func main() {
	var (
		wg            sync.WaitGroup
		mutex         sync.Mutex
		x             = 0
		routineNumber = 100
	)

	wg.Add(routineNumber)

	for i := 0; i < routineNumber; i++ {
		go func() {
			mutex.Lock()
			y := x
			y++
			x = y
			mutex.Unlock()
			wg.Done()
		}()
	}

	wg.Wait()

	fmt.Println(x)
}
