package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

func main() {
	var (
		wg            sync.WaitGroup
		x             int64 = 0
		routineNumber       = 100
	)

	wg.Add(routineNumber)
	for i := 0; i < routineNumber; i++ {
		go func() {
			atomic.AddInt64(&x, 1)
			wg.Done()
		}()
	}

	wg.Wait()

	fmt.Println(x)
}
