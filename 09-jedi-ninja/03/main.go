package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	var (
		wg            sync.WaitGroup
		x             = 0
		routineNumber = 100
	)

	wg.Add(routineNumber)

	for i := 0; i < routineNumber; i++ {
		go func() {
			y := x
			runtime.Gosched()
			y++
			x = y
			wg.Done()
		}()
	}

	wg.Wait()

	fmt.Println(x)
}
