package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Println("GO os:", runtime.GOOS)
	fmt.Println("GO arch:", runtime.GOARCH)
}
