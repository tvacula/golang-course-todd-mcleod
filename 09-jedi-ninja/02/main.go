package main

import "fmt"

type person struct {
	First string
	Last  string
	Age   int
}

func (p *person) speak() {
	fmt.Println("Hi from", p.First, p.Last)
}

type human interface {
	speak()
}

func saySomething(h human) {
	h.speak()
}

func main() {
	p := person{
		First: "Leif",
		Last:  "Ericksson",
		Age:   40,
	}

	//saySomething(p)
	saySomething(&p)
}
