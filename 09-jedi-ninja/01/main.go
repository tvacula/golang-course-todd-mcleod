package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup

func main() {
	fmt.Println("Main func began!")
	wg.Add(1)
	go func() {
		fmt.Println("First Goroutine !")
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		fmt.Println("Second Goroutine !")
		wg.Done()
	}()
	wg.Wait()
	fmt.Println("Main func Ended!")
}
