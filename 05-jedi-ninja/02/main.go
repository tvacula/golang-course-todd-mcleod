package main

import "fmt"

type person struct {
	firstName string
	lastName  string
	flavors   []string
}

func main() {
	p1 := person{
		firstName: "John",
		lastName:  "Constantine",
		flavors: []string{
			"Holy",
			"Unholy",
		},
	}

	p2 := person{
		firstName: "Johnathan",
		lastName:  "Wick",
		flavors: []string{
			"Happiness",
			"Revenge",
		},
	}

	m := map[string]person{
		p1.lastName: p1,
		p2.lastName: p2,
	}

	for k, person := range m {
		fmt.Printf("%v | %v %v\n", k, person.firstName, person.lastName)
		for i, v := range person.flavors {
			fmt.Printf("\t%v %v\n", i, v)
		}
	}
}
