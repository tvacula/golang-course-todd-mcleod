package main

import "fmt"

func main() {
	x := struct {
		names     []string
		favorites map[string][]string
	}{
		names: []string{"Carl von Bahnhoff", "Johnathan Frakes", "Bambi the Bimbo"},
		favorites: map[string][]string{
			"Ice cream": {"Chocolate", "Vanilla"},
			"Cars":      {"Volvo", "Volksvagen"},
		},
	}

	fmt.Println(x)
	fmt.Println(x.names)
	for _, v := range x.names {
		fmt.Printf("\t%v\n", v)
	}
	fmt.Println(x.favorites)
	for _, v := range x.favorites {
		fmt.Printf("\t%v\n", v)
	}
}
