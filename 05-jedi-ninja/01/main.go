package main

import "fmt"

type person struct {
	firstName string
	lastName  string
	flavors   []string
}

func main() {
	p1 := person{
		firstName: "John",
		lastName:  "Constantine",
		flavors: []string{
			"Holy",
			"Unholy",
		},
	}

	p2 := person{
		firstName: "Johnathan",
		lastName:  "Wick",
		flavors: []string{
			"Happiness",
			"Revenge",
		},
	}

	fmt.Println(p1)
	for _, v := range p1.flavors {
		fmt.Printf("\t%v\n", v)
	}

	fmt.Println(p2)
	for _, v := range p2.flavors {
		fmt.Printf("\t%v\n", v)
	}
}
