package main

import "fmt"

func main() {
	jb := []string{"James", "Bond", "Shaken, not stirred"}
	mp := []string{"Miss", "Moneypenny", "Heloooo, James"}

	x := [][]string{jb, mp}

	for i, v := range x {
		fmt.Println(i, v)
		for j, w := range v {
			fmt.Printf("\t%v %v\n", j, w)
		}
	}
}
