package main

import "fmt"

func main() {
	a := [5]int{0, 1, 2, 3, 42}

	for k, v := range a {
		fmt.Println(k, v)
	}

	fmt.Printf("%T\n", a)
}
