package main

import "fmt"

func main() {
	m := map[string][]string{
		"bond_james": {
			"Shaken, not stirred",
			"Martinis",
			"Women",
		},
		"moneypenny_miss": {
			"James Bond",
			"Literature",
			"Computer Science",
		},
		"no_dr": {
			"Being Evil",
			"Ice cream",
			"Sunsets",
		},
	}

	m["baggins_bilbo"] = []string{"The Ring", "Frodo", "Longbottom Leaf"}

	for k, v := range m {
		fmt.Println(k, v)
		for k2, v2 := range v {
			fmt.Printf("\t%v %v\n", k2, v2)
		}
	}
}
