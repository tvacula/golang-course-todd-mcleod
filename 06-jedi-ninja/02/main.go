package main

import "fmt"

func main() {
	x := []int{1, 2, 3, 4, 5, 6}
	fmt.Println(foo(x...))
	fmt.Println(bar(x))
}

func foo(p ...int) int {
	if len(p) == 1 {
		return p[0]
	}

	return p[0] + foo(p[1:]...)
}

func bar(p []int) int {
	total := 0
	for _, v := range p {
		total += v
	}

	return total
}
