package main

import "fmt"

func main() {
	defer foo()
	fmt.Println("This is a message that should be printed first.")
}

func foo() {
	fmt.Println("Wazzaap from a defer func")
}
