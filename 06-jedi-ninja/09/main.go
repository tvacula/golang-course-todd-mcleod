package main

import "fmt"

func main() {
	foo("ejhle", printAsLine)
	foo("ejhle", printAsFormat)
}

func foo(v string, f func(string)) {
	f(v)
}

func printAsLine(s string) {
	fmt.Println("priting value", s, "as a line")
}

func printAsFormat(s string) {
	fmt.Printf("priting value %v as format\n", s)
}
