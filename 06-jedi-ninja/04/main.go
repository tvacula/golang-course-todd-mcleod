package main

import "fmt"

type person struct {
	first string
	last  string
	age   uint
}

func (p person) speak() {
	fmt.Printf("My name is %v %v and I am %d\n", p.first, p.last, p.age)
}

func main() {
	p := person{
		first: "John",
		last:  "Constantine",
		age:   35,
	}

	p.speak()
}
