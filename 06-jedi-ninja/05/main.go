package main

import (
	"fmt"
	"math"
)

type square struct {
	side float64
}

type circle struct {
	radius float64
}

type shape interface {
	calculateArea() float64
}

func (s square) calculateArea() float64 {
	return math.Pow(s.side, 2)
}

func (c circle) calculateArea() float64 {
	return math.Pi * math.Pow(c.radius, 2)
}

func info(s shape) {
	fmt.Printf("The %T has an area of %v\n", s, s.calculateArea())
}

func main() {
	s := square{
		side: 4,
	}

	c := circle{
		radius: 5,
	}

	info(s)
	info(c)
}
