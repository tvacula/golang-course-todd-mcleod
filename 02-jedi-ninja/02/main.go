package main

import "fmt"

func main() {
	g := 5 == 6
	h := 5 <= 6
	i := 5 >= 6
	j := 5 != 6
	k := 5 > 6
	l := 5 < 6

	fmt.Println(g, h, i, j, k, l)
}
