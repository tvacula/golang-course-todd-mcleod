package main

import "fmt"

const (
	year2020 = 2020 - iota
	year2019 = 2020 - iota
	year2018 = 2020 - iota
	year2017 = 2020 - iota
)

func main() {
	fmt.Println(year2020, year2019, year2018, year2017)
}
