package main

import "fmt"

const (
	x int8 = 42
	y      = "James Bond"
)

func main() {
	fmt.Println(x, y)
}
