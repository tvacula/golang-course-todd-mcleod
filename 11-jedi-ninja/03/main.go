package main

import "fmt"

type customErr struct {
}

func (c customErr) Error() string {
	return "custom error has occurred!"
}

func main() {
	ce := customErr{}
	foo(ce)
}

func foo(e error) {
	fmt.Println(e.Error())
}
