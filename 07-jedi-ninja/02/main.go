package main

import "fmt"

type person struct {
	name    string
	address string
}

func main() {
	p := person{
		name:    "Karel",
		address: "Někde 123",
	}

	fmt.Println(p)

	changeMe(&p, "Jinde 321")

	fmt.Println(p)
}

func changeMe(p *person, address string) {
	p.address = address
}
